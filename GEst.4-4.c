//GUIA ESTRUCTURAS EJERCICIO 4
//Una ferreteria tiene un listado de facturas emitidas en cierto a�o, con estos datos: 
//numero de factura, fecha de emision (dia y mes), nombre del cliente y monto total.
//Dado un vector de 10 facturas, se necesita mostrar en pantalla cu�l fue el mejor mes (o sea, el de mayor dinero facturado).
//Los datos se pueden ingresar por teclado o dejarlos fijos en el programa para no perder tiempo en tipear datos.

#include <stdio.h>

struct facturas
{
    int num;
    int dia;
    float monto;
    char mes[20];
    char nombre_cliente [20];
};

//Declaro la estructura facturas y los datos que incluira.

struct facturas factura[10] = {
                            {1, 1, 19531.25, "Marzo", "Nick"},
                            {2, 2, 39062.5, "Abril", "Connor"},
                            {3, 3, 78125.0, "Mayo", "Nathan"},
                            {4, 4, 156250.0, "Junio", "Ben"},
                            {5, 5, 312500.0, "Julio", "Charlie"},
                            {6, 6, 625000.0, "Agosto", "Josh"},
                            {7, 7, 1250000.0, "Septiembre", "Jack"},
                            {8, 8, 2500000.0, "Octubre", "Ashley"},
                            {9, 9, 5000000.0, "Noviembre", "Dwight"},
                            {10, 10, 10000000.0, "Diciembre", "Matej"}
};

//Declaro la estructura facturas y con los datos de las facturas (numero de factura, fecha de emision (dia y mes), nombre del cliente y monto total).

int main(){

    int i;
    int mayor = 0;
    int j;
    
    for(i=0;i<10;i++){
        if(factura[i].monto > mayor){
            mayor = factura[i].monto;
            j = i;
        }
    }
    
//Comparo los facturas de los meses, y la mayor la muestro en pantalla.

printf("El mejor mes fue %s",factura[j].mes);

}